package com.e.countrylist.Model;

public class Paises {
    private String id;
    private String nombre;
    private String capital;
    private String poblacion;
    private String urlBandera;
    private String himno;


    public Paises(){

    }

    public Paises(String id, String nombre, String capital, String poblacion,String url, String himno) {

        this.id = id;
        this.nombre = nombre;
        this.capital = capital;
        this.poblacion= poblacion;
        this.urlBandera = url;
        this.himno = himno;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getUrlBandera() {
        return urlBandera;
    }

    public void setUrlBandera(String urlBandera) {
        this.urlBandera = urlBandera;
    }

    public String getPoblacion() {
        return poblacion;
    }

    public void setPoblacion(String poblacion) {
        this.poblacion = poblacion;
    }
}
